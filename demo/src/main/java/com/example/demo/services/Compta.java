package com.example.demo.services;

import java.util.Map;

public class Compta {

	
	private int value = 0;
	
	public int add(int v) {
		return value + v;
	}
	
	
	public int remove(int v) {
		if (v > value) throw new IllegalArgumentException("valeur negative");
			return value - v;
	}
	
	
	public boolean reset() {
		value = 0;
		return true;
	}
	

}
